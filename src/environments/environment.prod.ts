export const environment = {
  production: true,
  organization: 'angular',
  githubApi: 'https://api.github.com'
};
