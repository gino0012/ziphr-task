import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { GithubApiService } from './common/services/githubApi.service';
import { MockGithubApiService } from './common/mocks/mock-github-api-service';

describe('AppComponent', () => {
  let mockGithubApiService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: GithubApiService, useClass: MockGithubApiService }
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    mockGithubApiService = TestBed.inject(GithubApiService);
    mockGithubApiService.getOrgMembers.and.returnValue(of([]));
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
