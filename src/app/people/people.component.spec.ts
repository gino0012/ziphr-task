import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeopleComponent } from './people.component';
import { GithubApiService } from '../common/services/githubApi.service';
import { MockGithubApiService } from '../common/mocks/mock-github-api-service';
import { SearchMemberPipe } from '../common/pipes/search-member.pipe';

describe('PeopleComponent', () => {
  let component: PeopleComponent;
  let fixture: ComponentFixture<PeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PeopleComponent,
        SearchMemberPipe
      ],
      providers: [
        { provide: GithubApiService, useClass: MockGithubApiService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
