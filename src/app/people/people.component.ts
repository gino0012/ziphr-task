import { Component, OnInit, Input } from '@angular/core';
import { GithubApiService } from '../common/services/githubApi.service';
import { OrgMember } from '../common/model/orgMember.model';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {
  @Input() members: OrgMember[];
  searchQuery = '';

  constructor(public githubApiService: GithubApiService) { }

  ngOnInit(): void {
  }
}
