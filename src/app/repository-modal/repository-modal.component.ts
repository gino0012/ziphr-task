import { Component, OnInit } from '@angular/core';
import { GithubApiService } from '../common/services/githubApi.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RepoCommit } from '../common/model/repoCommit.model';
import { RepoIssue } from '../common/model/repoIssue.modal';

@Component({
  selector: 'app-repository-modal',
  templateUrl: './repository-modal.component.html',
  styleUrls: ['./repository-modal.component.css']
})
export class RepositoryModalComponent implements OnInit {
  repoName: string;
  readmeContent: string;
  commits: RepoCommit[];
  issues: RepoIssue[];
  searchQuery = '';
  orderCommitsBy = 'newer';
  orderIssuesBy = 'newer';

  constructor(
    private githubApiService: GithubApiService,
    public modalRef: BsModalRef) {}

  ngOnInit(): void {
    this.githubApiService.getRepoReadme(this.repoName).subscribe(content => this.readmeContent = content);
    this.githubApiService.getRepoCommits(this.repoName).subscribe(commits => this.commits = commits);
    this.githubApiService.getRepoIssues(this.repoName).subscribe(issues => this.issues = issues);
  }

  setOrderCommitsBy(order: string): void {
    this.orderCommitsBy = order;
  }

  setOrderIssuesBy(order: string): void {
    this.orderIssuesBy = order;
  }
}
