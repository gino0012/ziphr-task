import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { RepositoryModalComponent } from './repository-modal.component';
import { GithubApiService } from '../common/services/githubApi.service';
import { MockGithubApiService } from '../common/mocks/mock-github-api-service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderCommitsPipe } from '../common/pipes/order-commits.pipe';
import { OrderIssuesPipe } from '../common/pipes/order-issues.pipe';
import { SearchIssuePipe } from '../common/pipes/search-issue.pipe';

describe('RepositoryModalComponent', () => {
  let component: RepositoryModalComponent;
  let fixture: ComponentFixture<RepositoryModalComponent>;
  let mockGithubApiService;
  const mockOrderBy = 'older';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RepositoryModalComponent,
        OrderCommitsPipe,
        OrderIssuesPipe,
        SearchIssuePipe
      ],
      providers: [
        { provide: GithubApiService, useClass: MockGithubApiService },
        { provide: BsModalRef, useValue: {}}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    mockGithubApiService = TestBed.inject(GithubApiService);
    mockGithubApiService.getRepoReadme.and.returnValue(of(''));
    mockGithubApiService.getRepoCommits.and.returnValue(of([]));
    mockGithubApiService.getRepoIssues.and.returnValue(of([]));
    fixture = TestBed.createComponent(RepositoryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set order by commits', () => {
    component.setOrderCommitsBy(mockOrderBy);

    expect(component.orderCommitsBy).toBe(mockOrderBy);
  });

  it('should set order by issues', () => {
    component.setOrderIssuesBy(mockOrderBy);

    expect(component.orderIssuesBy).toBe(mockOrderBy);
  });
});
