export class MockGithubApiService {
    getOrgDetails = jasmine.createSpy('organization get details');
    getOrgMembers = jasmine.createSpy('organization get members');
    getOrgRepos = jasmine.createSpy('organization get repositories');
    getRepoReadme = jasmine.createSpy('repository get readme');
    getRepoCommits = jasmine.createSpy('repository get commits');
    getRepoIssues = jasmine.createSpy('repository get issues');
}
