import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { environment } from '../../../environments/environment';
import { GithubApiService } from './githubApi.service';
import { OrgDetails } from '../model/orgDetails.model';
import { OrgMember } from '../model/orgMember.model';
import { OrgRepo } from '../model/orgRepo.model';
import { RepoReadme } from '../model/repoReadme.model';
import { RepoCommit } from '../model/repoCommit.model';
import { RepoIssue } from '../model/repoIssue.modal';

describe('GithubApiService', () => {
  let service: GithubApiService;
  let httpMock: HttpTestingController;
  const orgUrl = `${environment.githubApi}/orgs/${environment.organization}`;
  const repoUrl = `${environment.githubApi}/repos/${environment.organization}`;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(GithubApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should get organization details', () => {
    const mockDetails: OrgDetails = {
      name: 'mock name',
      public_repos: 1234,
      avatar_url: 'http://avatar/url'
    };

    service.getOrgDetails().subscribe(details => {
      expect(details).toEqual(mockDetails);
    });

    const req = httpMock.expectOne(orgUrl);
    expect(req.request.method).toBe('GET');
    req.flush(mockDetails);
  });

  it('should get organization members', () => {
    const mockName = {
      name: 'John Doe'
    };
    const mockMembers: OrgMember[] = [{
      id: 1,
      login: 'jdoe',
      avatar_url: 'http://avatar/url',
      url: 'http://member/url'
    },
    {
      id: 2,
      login: 'jdoe',
      avatar_url: 'http://avatar/url2',
      url: 'http://member/url'
    }];

    service.getOrgMembers().subscribe(members => {
      expect(members.length).toBe(2);
      expect(members).toEqual(mockMembers.map((member) => {
        return {
          ...member,
          name: mockName.name
        };
      }));
    });

    const reqMembers = httpMock.expectOne(`${orgUrl}/members`);
    expect(reqMembers.request.method).toBe('GET');
    reqMembers.flush(mockMembers);

    const requestsName = httpMock.match(mockMembers[0].url);
    expect(requestsName.length).toBe(2);
    expect(requestsName[0].request.method).toBe('GET');
    expect(requestsName[1].request.method).toBe('GET');
    requestsName[0].flush(mockName);
    requestsName[1].flush(mockName);
  });

  it('should get organization repositories', () => {
    const mockRepos: OrgRepo[] = [{
      id: 1,
      name: 'repo 1',
      description: 'sample description',
      stargazers_count: 1234,
      language: 'java'
    },
    {
      id: 2,
      name: 'repo 2',
      description: 'sample description',
      stargazers_count: 1234,
      language: 'ruby'
    }];

    service.getOrgRepos().subscribe(repos => {
      expect(repos.length).toBe(2);
      expect(repos).toEqual(mockRepos);
    });

    const req = httpMock.expectOne(`${orgUrl}/repos`);
    expect(req.request.method).toBe('GET');
    req.flush(mockRepos);
  });

  it('should get repository readme', () => {
    const mockRepoName = 'repo-name';
    const mockContent = 'sample readme content';
    const mockRes: RepoReadme = {
      content: btoa(mockContent)
    };

    service.getRepoReadme(mockRepoName).subscribe(content => {
      expect(content).toEqual(mockContent);
    });

    const req = httpMock.expectOne(`${repoUrl}/${mockRepoName}/readme`);
    expect(req.request.method).toBe('GET');
    req.flush(mockRes);
  });

  it('should get repository commits', () => {
    const mockRepoName = 'repo-name';
    const mockCommits: RepoCommit[] = [{
      commit: {
        author: {
          date: 12345678
        },
        message: 'sample message'
      }
    }];

    service.getRepoCommits(mockRepoName).subscribe(commits => {
      expect(commits).toEqual(mockCommits);
    });

    const req = httpMock.expectOne(`${repoUrl}/${mockRepoName}/commits`);
    expect(req.request.method).toBe('GET');
    req.flush(mockCommits);
  });

  it('should get repository issues', () => {
    const mockRepoName = 'repo-name';
    const mockIssues: RepoIssue[] = [{
      title: 'sample title',
      created_at: 123245556,
      user: {
        login: 'jdoe'
      }
    }];

    service.getRepoIssues(mockRepoName).subscribe(issues => {
      expect(issues).toEqual(mockIssues);
    });

    const req = httpMock.expectOne(`${repoUrl}/${mockRepoName}/issues`);
    expect(req.request.method).toBe('GET');
    req.flush(mockIssues);
  });
});
