import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { pluck, switchMap, map } from 'rxjs/operators';
import { OrgDetails } from '../model/orgDetails.model';
import { OrgMember } from '../model/orgMember.model';
import { OrgRepo } from '../model/orgRepo.model';
import { RepoReadme } from '../model/repoReadme.model';
import { RepoCommit } from '../model/repoCommit.model';
import { RepoIssue } from '../model/repoIssue.modal';

@Injectable({
  providedIn: 'root'
})
export class GithubApiService {
  private orgUrl: string;
  private repoUrl: string;

  constructor(private http: HttpClient) {
    this.orgUrl = `${environment.githubApi}/orgs/${environment.organization}`;
    this.repoUrl = `${environment.githubApi}/repos/${environment.organization}`;
  }

  getOrgDetails(): Observable<OrgDetails> {
    return this.http.get<OrgDetails>(this.orgUrl);
  }

  getOrgMembers(): Observable<OrgMember[]> {
    return this.http.get<OrgMember[]>(`${this.orgUrl}/members`, this.createHeader()).pipe(
      switchMap((members: OrgMember[]) => Promise.all(members.map(async (member: OrgMember) => {
        const name = await this.getMemberName(member.url).toPromise();
        return {
          ...member,
          name
        };
      })))
    );
  }

  getOrgRepos(): Observable<OrgRepo[]>{
    return this.http.get<OrgRepo[]>(`${this.orgUrl}/repos`, this.createHeader());
  }

  getRepoReadme(repoName: string): Observable<string>{
    return this.http.get<RepoReadme>(`${this.repoUrl}/${repoName}/readme`, this.createHeader()).pipe(
      map((data: RepoReadme) => atob(data.content))
    );
  }

  getRepoCommits(repoName: string): Observable<RepoCommit[]>{
    return this.http.get<RepoCommit[]>(`${this.repoUrl}/${repoName}/commits`, this.createHeader()).pipe(
      map((commits: RepoCommit[]) => commits.map((commit: RepoCommit) => {
        return {
          commit: {
            author: {
              date: +new Date(commit.commit.author.date)
            },
            message: commit.commit.message
          }
        };
      }))
    );
  }

  getRepoIssues(repoName: string): Observable<RepoIssue[]>{
    return this.http.get<RepoIssue[]>(`${this.repoUrl}/${repoName}/issues`, this.createHeader()).pipe(
      map((issues: RepoIssue[]) => issues.map((issue: RepoIssue) => {
        return {
            created_at: +new Date(issue.created_at),
            user: {
              login: issue.user.login,
            },
            title: issue.title
        };
      }))
    );
  }

  private getMemberName(url: string): Observable<string>{
    return this.http.get(url, this.createHeader()).pipe(
      pluck('name')
    );
  }

  private createHeader(): {headers: {}} {
    return {
      headers: {
        Authorization: 'Token 434a00e11a40b28889c589d73b7ae0416355fb76'
      }
    };
  }
}
