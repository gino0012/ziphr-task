import { Pipe, PipeTransform } from '@angular/core';
import { OrgRepo } from '../model/orgRepo.model';

@Pipe({
  name: 'searchRepository'
})
export class SearchRepositoryPipe implements PipeTransform {

  transform(repos: OrgRepo[], searchQuery: string, searchLang: string): OrgRepo[] {
    if (searchLang !== 'All') {
      repos = repos.filter((repo: OrgRepo) =>  {
        return repo.language === searchLang;
      });
    }
    if (searchQuery.length < 3) {
      return repos;
    }
    return repos.filter((repo: OrgRepo) =>  {
      return repo.name.toLowerCase().includes(searchQuery);
    });
  }

}
