import { Pipe, PipeTransform } from '@angular/core';
import { RepoIssue } from '../model/repoIssue.modal';

@Pipe({
  name: 'orderIssues'
})
export class OrderIssuesPipe implements PipeTransform {

  transform(issues: RepoIssue[], orderBy: string): RepoIssue[] {
    if (issues) {
      if (orderBy === 'newer') {
        return issues.sort((a, b) => (a.created_at > b.created_at) ? 1 : -1).reverse();
      }
      return issues.sort((a, b) => (a.created_at > b.created_at) ? 1 : -1);
    }
    return issues;
  }
}
