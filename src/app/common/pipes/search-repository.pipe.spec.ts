import { SearchRepositoryPipe } from './search-repository.pipe';
import { OrgRepo } from '../model/orgRepo.model';

describe('SearchRepositoryPipe', () => {
  let pipe: SearchRepositoryPipe;
  let mockRepos: OrgRepo[];

  beforeEach(() => {
    pipe = new SearchRepositoryPipe();
    mockRepos = [{
      description: 'sample description',
      id: 1,
      name: 'repo-name',
      stargazers_count: 5,
      language: 'java'
    }, {
      description: 'sample description 1',
      id: 2,
      name: 'repo-name1',
      stargazers_count: 10,
      language: 'ruby'
    }, {
      description: 'sample description 2',
      id: 3,
      name: 'repo-name2',
      stargazers_count: 103,
      language: 'ruby'
    }];
  });

  it('should search repos when searchQuery is empty', () => {
    const result = pipe.transform(mockRepos, '', 'All');

    expect(result).toEqual(mockRepos);
  });

  it('should search repos when searchQuery is not empty', () => {
    const result = pipe.transform(mockRepos, 'name1', 'All');
    expect(result).toEqual([mockRepos[1]]);
  });

  it('should search repos when searchLang is not All', () => {
    const result = pipe.transform(mockRepos, 'repo', 'ruby');
    expect(result).toEqual([mockRepos[1], mockRepos[2]]);
  });
});
