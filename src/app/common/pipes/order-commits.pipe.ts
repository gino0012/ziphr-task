import { Pipe, PipeTransform } from '@angular/core';
import { RepoCommit } from '../model/repoCommit.model';

@Pipe({
  name: 'orderCommits'
})
export class OrderCommitsPipe implements PipeTransform {

  transform(commits: RepoCommit[], orderBy: string): RepoCommit[] {
    if (commits) {
      if (orderBy === 'newer') {
        return commits.sort((a, b) => (a.commit.author.date > b.commit.author.date) ? 1 : -1).reverse();
      }
      return commits.sort((a, b) => (a.commit.author.date > b.commit.author.date) ? 1 : -1);
    }
    return commits;
  }
}
