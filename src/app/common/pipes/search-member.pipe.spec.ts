import { SearchMemberPipe } from './search-member.pipe';
import { OrgMember } from '../model/orgMember.model';

describe('SearchMemberPipe', () => {
  let pipe: SearchMemberPipe;
  let mockMembers: OrgMember[];

  beforeEach(() => {
    pipe = new SearchMemberPipe();
    mockMembers = [{
      id: 1,
      avatar_url: 'avatar/url',
      login: 'jdoe',
      url: 'member/url'
    }, {
      id: 1,
      avatar_url: 'avatar/url',
      login: 'jcruz',
      url: 'member/url',
      name: 'juan cruz'
    }];
  });

  it('should search member when searchQuery is empty', () => {
    const result = pipe.transform(mockMembers, '');

    expect(result).toEqual(mockMembers);
  });

  it('should search member when searchQuery is not empty', () => {
    const result = pipe.transform(mockMembers, 'jcru');
    expect(result).toEqual([mockMembers[1]]);
  });
});
