import { OrderIssuesPipe } from './order-issues.pipe';
import { RepoIssue } from '../model/repoIssue.modal';

describe('OrderIssuesPipe', () => {
  let pipe: OrderIssuesPipe;
  let mockIssues: RepoIssue[];

  beforeEach(() => {
    pipe = new OrderIssuesPipe();
    mockIssues = [{
      user: {
        login: 'jdoe'
      },
      title: 'sample title',
      created_at: 3
    }, {
      user: {
        login: 'jdoe1'
      },
      title: 'sample title',
      created_at: 1
    }, {
      user: {
        login: 'jdoe2'
      },
      title: 'sample title',
      created_at: 2
    }];
  });
  it('should return null if no commits passed', () => {
    expect(pipe.transform(null, 'older')).toBe(null);
  });

  it('should order commit by older', () => {
    expect(pipe.transform([...mockIssues], 'older')).toEqual([mockIssues[1], mockIssues[2], mockIssues[0]]);
  });

  it('should order commit by newer', () => {
    expect(pipe.transform([...mockIssues], 'newer')).toEqual([mockIssues[0], mockIssues[2], mockIssues[1]]);
  });
});
