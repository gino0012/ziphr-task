import { SearchIssuePipe } from './search-issue.pipe';
import { RepoIssue } from '../model/repoIssue.modal';

describe('SearchIssuePipe', () => {
  let pipe: SearchIssuePipe;
  let mockIssues: RepoIssue[];

  beforeEach(() => {
    pipe = new SearchIssuePipe();
    mockIssues = [{
      user: {
        login: 'jdoe'
      },
      title: 'sample title1',
      created_at: 3
    }, {
      user: {
        login: 'jdoe1'
      },
      title: 'sample title2',
      created_at: 1
    }, {
      user: {
        login: 'jdoe2'
      },
      title: 'sample title3',
      created_at: 2
    }];
  });

  it('should search issue when searchQuery is empty', () => {
    const result = pipe.transform(mockIssues, '');

    expect(result).toEqual(mockIssues);
  });

  it('should search issue when searchQuery is not empty', () => {
    const result = pipe.transform(mockIssues, 'title2');
    expect(result).toEqual([mockIssues[1]]);
  });
});
