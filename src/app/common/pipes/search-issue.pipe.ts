import { Pipe, PipeTransform } from '@angular/core';
import { RepoIssue } from '../model/repoIssue.modal';

@Pipe({
  name: 'searchIssue'
})
export class SearchIssuePipe implements PipeTransform {

  transform(issues: RepoIssue[], searchQuery: string): RepoIssue[] {
    if (searchQuery.length < 3) {
      return issues;
    }
    return issues.filter((issue: RepoIssue) =>  {
      return issue.title.toLowerCase().includes(searchQuery.toLowerCase());
    });
  }

}
