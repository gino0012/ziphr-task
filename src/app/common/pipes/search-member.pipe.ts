import { Pipe, PipeTransform } from '@angular/core';
import { OrgMember } from '../model/orgMember.model';

@Pipe({
  name: 'searchMember'
})
export class SearchMemberPipe implements PipeTransform {

  transform(members: OrgMember[], searchQuery: string): OrgMember[] {
    if (searchQuery.length < 3) {
      return members;
    }
    return members.filter((member: OrgMember) =>  {
      const isMatchLogin = member.login.toLowerCase().includes(searchQuery.toLowerCase());
      if (member.name) {
        return member.name.toLowerCase().includes(searchQuery.toLowerCase()) || isMatchLogin;
      }
      return isMatchLogin;
    });
  }
}
