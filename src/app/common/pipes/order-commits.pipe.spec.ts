import { OrderCommitsPipe } from './order-commits.pipe';
import { RepoCommit } from '../model/repoCommit.model';

describe('OrderCommitsPipe', () => {
  let pipe: OrderCommitsPipe;
  let mockCommits: RepoCommit[];

  beforeEach(() => {
    pipe = new OrderCommitsPipe();
    mockCommits = [{
      commit: {
        author: {
          date: 3
        },
        message: 'sample message'
      }
    }, {
      commit: {
        author: {
          date: 1
        },
        message: 'sample message'
      }
    }, {
      commit: {
        author: {
          date: 2
        },
        message: 'sample message'
      }
    }];
  });
  it('should return null if no commits passed', () => {
    expect(pipe.transform(null, 'older')).toBe(null);
  });

  it('should order commit by older', () => {
    expect(pipe.transform([...mockCommits], 'older')).toEqual([mockCommits[1], mockCommits[2], mockCommits[0]]);
  });

  it('should order commit by newer', () => {
    expect(pipe.transform([...mockCommits], 'newer')).toEqual([mockCommits[0], mockCommits[2], mockCommits[1]]);
  });
});
