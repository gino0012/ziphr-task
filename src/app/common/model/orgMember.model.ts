export interface OrgMember {
    id: number;
    login: string;
    avatar_url: string;
    url: string;
    name?: string;
}
