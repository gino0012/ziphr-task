export interface OrgRepo {
    id: number;
    name: string;
    description: string;
    stargazers_count: number;
    language: string;
}
