export interface RepoCommit {
    commit: {
        author: {date: number},
        message: string,
    };
}
