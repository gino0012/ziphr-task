export interface OrgDetails {
    name: string;
    public_repos: number;
    avatar_url: string;
}
