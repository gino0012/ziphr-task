export interface RepoIssue {
    title: string;
    created_at: number;
    user: {
        login: string,
    };
}
