import { Component, OnInit, TemplateRef } from '@angular/core';
import { GithubApiService } from '../common/services/githubApi.service';
import { OrgRepo } from '../common/model/orgRepo.model';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { RepositoryModalComponent } from '../repository-modal/repository-modal.component';

@Component({
  selector: 'app-repositories',
  templateUrl: './repositories.component.html',
  styleUrls: ['./repositories.component.css']
})
export class RepositoriesComponent implements OnInit {
  faStar = faStar;
  repos: OrgRepo[] = [];
  modalRef: BsModalRef;
  searchQuery = '';
  searchLang = 'All';

  constructor(private githubApiService: GithubApiService, private modalService: BsModalService) {
    this.githubApiService.getOrgRepos().subscribe((repos: OrgRepo[]) => this.repos = repos);
  }

  ngOnInit(): void {
  }

  openModal(repoName: string): void {
    const initialState = {
      repoName
    };
    this.modalRef = this.modalService.show(RepositoryModalComponent, {initialState, class: 'modal-lg'});
  }

  getLanguages(): string[] {
    const languages = this.repos.map((repo: OrgRepo) => repo.language);
    return languages.filter((lang, index) => lang && languages.indexOf(lang) === index);
  }
}
