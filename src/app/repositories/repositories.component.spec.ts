import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { RepositoriesComponent } from './repositories.component';
import { GithubApiService } from '../common/services/githubApi.service';
import { MockGithubApiService } from '../common/mocks/mock-github-api-service';
import { MockBsModalService } from '../common/mocks/mock-bs-modal-service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { TemplateRef } from '@angular/core';
import { RepositoryModalComponent } from '../repository-modal/repository-modal.component';
import { SearchMemberPipe } from '../common/pipes/search-member.pipe';
import { SearchRepositoryPipe } from '../common/pipes/search-repository.pipe';
import { OrgRepo } from '../common/model/orgRepo.model';

describe('RepositoriesComponent', () => {
  let component: RepositoriesComponent;
  let fixture: ComponentFixture<RepositoriesComponent>;
  let mockGithubApiService;
  let mockBsModalService;
  let mockRepos: OrgRepo[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RepositoriesComponent,
        SearchRepositoryPipe
      ],
      providers: [
        { provide: GithubApiService, useClass: MockGithubApiService },
        { provide: BsModalService, useClass: MockBsModalService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    mockRepos = [{
      id: 1,
      description: 'sample description',
      language: 'java',
      stargazers_count: 123,
      name: 'repo-name'
    }, {
      id: 2,
      description: 'sample description',
      language: 'python',
      stargazers_count: 123,
      name: 'repo-name'
    }, {
      id: 3,
      description: 'sample description',
      language: 'python',
      stargazers_count: 123,
      name: 'repo-name'
    }];

    mockGithubApiService = TestBed.inject(GithubApiService);
    mockBsModalService = TestBed.inject(BsModalService);
    mockGithubApiService.getOrgRepos.and.returnValue(of(mockRepos));
    fixture = TestBed.createComponent(RepositoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should open modal', () => {
    const mockRepoName = 'repo-name';

    component.openModal(mockRepoName);

    expect(mockBsModalService.show).toHaveBeenCalledWith(jasmine.any(Function), {
      initialState: {
        repoName: mockRepoName
      },
      class: 'modal-lg'
    });
  });

  it('should get languages', () => {
    expect(component.getLanguages()).toEqual(['java', 'python']);
  });
});
