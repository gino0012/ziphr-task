import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';

import { GithubApiService } from './common/services/githubApi.service';
import { RepositoriesComponent } from './repositories/repositories.component';
import { PeopleComponent } from './people/people.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RepositoryModalComponent } from './repository-modal/repository-modal.component';
import { SearchMemberPipe } from './common/pipes/search-member.pipe';
import { SearchRepositoryPipe } from './common/pipes/search-repository.pipe';
import { OrderCommitsPipe } from './common/pipes/order-commits.pipe';
import { OrderIssuesPipe } from './common/pipes/order-issues.pipe';
import { SearchIssuePipe } from './common/pipes/search-issue.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RepositoriesComponent,
    PeopleComponent,
    RepositoryModalComponent,
    SearchMemberPipe,
    SearchRepositoryPipe,
    OrderCommitsPipe,
    OrderIssuesPipe,
    SearchIssuePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FontAwesomeModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    FormsModule
  ],
  providers: [GithubApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
