import { Component } from '@angular/core';
import { setTheme } from 'ngx-bootstrap/utils';
import { GithubApiService } from './common/services/githubApi.service';
import { OrgMember } from './common/model/orgMember.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  members;

  constructor(private githubApiService: GithubApiService) {
    setTheme('bs4');
    this.githubApiService.getOrgMembers().subscribe((members) => this.members = members);
  }
}
