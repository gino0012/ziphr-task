import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { HeaderComponent } from './header.component';
import { GithubApiService } from '../common/services/githubApi.service';
import { MockGithubApiService } from '../common/mocks/mock-github-api-service';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let mockGithubApiService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],
      providers: [
        {provide: GithubApiService, useClass: MockGithubApiService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    mockGithubApiService = TestBed.inject(GithubApiService);
    mockGithubApiService.getOrgDetails.and.returnValue(of({}));

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();


  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
