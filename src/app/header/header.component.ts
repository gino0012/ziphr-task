import { Component, OnInit, Input } from '@angular/core';
import { GithubApiService } from '../common/services/githubApi.service';
import { OrgDetails } from '../common/model/orgDetails.model';
import { OrgMember } from '../common/model/orgMember.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() members: OrgMember[];
  details: OrgDetails;

  constructor(private githubApiService: GithubApiService) {
    this.githubApiService.getOrgDetails().subscribe((details: OrgDetails) => this.details = details);
  }

  ngOnInit(): void {
  }

}
