[![pipeline status](https://gitlab.com/gino0012/ziphr-task/badges/master/pipeline.svg)](https://gitlab.com/gino0012/ziphr-task/-/commits/master) [![coverage report](https://gitlab.com/gino0012/ziphr-task/badges/master/coverage.svg)](https://gitlab.com/gino0012/ziphr-task/-/commits/master)

# ZiphrTask

Deployed: https://ziphr-task.web.app/

## Development server

Run `npm run develop` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project.

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running  verification before commit

Run `npm run verify` to execute lint, build, test before pushing changes.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
